
# Consumer code
import pika
import os
import time
import logging

logging.basicConfig()

def pdf_process_function(msg):
    print("PDF processing")
    print("[x] Received " + str(msg))

    time.sleep(5)  # Simulate PDF processing time
    print("PDF processing finished")

# Access the CLOUDAMQP_URL environment variable and parse it (fallback to localhost)
url = os.environ.get('CLOUDAMQP_URL', 'amqp://huy:NTH@13042000@localhost/')
params = pika.URLParameters(url)
connection = None  # Initialize connection variable

try:
    # Connect to RabbitMQ
    connection = pika.BlockingConnection(params)
    channel = connection.channel()  # Start a channel
    channel.queue_declare(queue='dos_queue', durable=True)  # Declare a queue

    # Define callback function
    def callback(ch, method, properties, body):
        pdf_process_function(body)

    # Set up subscription on the queue
    channel.basic_consume('dos_queue', callback, auto_ack=True)

    print('Waiting for messages. To exit press CTRL+C')

    # Start consuming (blocks)
    channel.start_consuming()
except pika.exceptions.AMQPConnectionError:
    print("Failed to connect to RabbitMQ")
except KeyboardInterrupt:
    print("Consumer stopped by user")
finally:
    # Close connection
    if connection and connection.is_open:
        connection.close()
