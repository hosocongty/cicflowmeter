import pika
import os
import logging
import time

logging.basicConfig()

# Parse CLOUDAMQP_URL (fallback to localhost)
url = os.environ.get('CLOUDAMQP_URL', 'amqp://huy:NTH@13042000@localhost/')
params = pika.URLParameters(url)
params.socket_timeout = 5

# Specify the virtual host explicitly
params.virtual_host = '/'

try:
    # Connect to RabbitMQ
    connection = pika.BlockingConnection(params)
    print("Successfully connected to RabbitMQ")

    # Create a new channel
    channel = connection.channel()

    # Ensure the queue exists
    channel.queue_declare(queue='dos_queue', durable=True)

    while True:
        # Send a message
        channel.basic_publish(exchange='dos_exchange', routing_key='dos_process', body='User information', properties=pika.BasicProperties(delivery_mode=2))  # Set message persistence
        print("[x] Message sent to consumer")

        # Sleep for 1 second
        time.sleep(1)

except pika.exceptions.AMQPConnectionError:
    print("Failed to connect to RabbitMQ")

finally:
    # Close the connection
    if connection and connection.is_open:
        connection.close()
