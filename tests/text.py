import pika
import os
import logging
import time

logging.basicConfig()

# Parse CLOUDAMQP_URL (fallback to localhost)
url = os.environ.get('CLOUDAMQP_URL', 'amqp://huy:NTH@13042000@localhost/')
params = pika.URLParameters(url)
params.socket_timeout = 5

# Specify the virtual host explicitly
params.virtual_host = '/'

try:
    # Connect to RabbitMQ
    connection = pika.BlockingConnection(params)
    print("Successfully connected to RabbitMQ")

    # Create a new channel
    channel = connection.channel()

    # Ensure the queue exists
    channel.queue_declare(queue='dos_queue', durable=True)

    while True:
        # Send a message
        channel.basic_publish(exchange='', routing_key='dos_queue', body='User information', properties=pika.BasicProperties(delivery_mode=2))  # Set message persistence
        print("[x] Message sent to consumer")

        # Sleep for 1 second
        time.sleep(1)

except pika.exceptions.AMQPConnectionError:
    print("Failed to connect to RabbitMQ")

finally:
    # Close the connection
    if connection and connection.is_open:
        connection.close()


# Consumer code
import pika
import os
import time
import logging

logging.basicConfig()

def pdf_process_function(msg):
    print("PDF processing")
    print("[x] Received " + str(msg))

    time.sleep(5)  # Simulate PDF processing time
    print("PDF processing finished")

# Access the CLOUDAMQP_URL environment variable and parse it (fallback to localhost)
url = os.environ.get('CLOUDAMQP_URL', 'amqp://huy:NTH@13042000@localhost/')
params = pika.URLParameters(url)
connection = None  # Initialize connection variable

try:
    # Connect to RabbitMQ
    connection = pika.BlockingConnection(params)
    channel = connection.channel()  # Start a channel
    channel.queue_declare(queue='dos_queue', durable=True)  # Declare a queue

    # Define callback function
    def callback(ch, method, properties, body):
        pdf_process_function(body)

    # Set up subscription on the queue
    channel.basic_consume('dos_queue', callback, auto_ack=True)

    print('Waiting for messages. To exit press CTRL+C')

    # Start consuming (blocks)
    channel.start_consuming()
except pika.exceptions.AMQPConnectionError:
    print("Failed to connect to RabbitMQ")
except KeyboardInterrupt:
    print("Consumer stopped by user")
finally:
    # Close connection
    if connection and connection.is_open:
        connection.close()
