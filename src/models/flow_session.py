from scapy.packet import Packet
from scapy.sessions import DefaultSession
from netaddr import IPAddress, IPNetwork
import json
from ipaddress import ip_address

# from writer import output_writer_factory
import os
import logging
import time
from constants.constants import EXPIRED_UPDATE, GARBAGE_COLLECT_PACKETS
from features.context import PacketDirection, get_packet_flow_key
from models.flow import Flow
from constants.utils import get_logger
from services.rabbitmq import push_data_rabbitmq
# Parse CLOUDAMQP_URL (fallback to localhost)
# url = os.environ.get('CLOUDAMQP_URL', 'amqp://huy:NTH@13042000@localhost/')
# params = pika.URLParameters(url)
# params.socket_timeout = 5

# Specify the virtual host explicitly
# params.virtual_host = '/'


class FlowSession(DefaultSession):
    """Creates a list of network flows."""

    def __init__(self, *args, **kwargs):
        self.flows = {}
        self.logger = get_logger(self.verbose)
        self.packets_count = 0
        # self.output_writer = output_writer_factory(self.output_mode, self.output_file)

        super(FlowSession, self).__init__(*args, **kwargs)

    def toPacketList(self):
        # Sniffer finished all the packets it needed to sniff.
        # It is not a good place for this, we need to somehow define a finish signal for AsyncSniffer
        self.garbage_collect(None)
        del self.output_writer
        return super(FlowSession, self).toPacketList()

    def on_packet_received(self, packet: Packet):
        count = 0
        direction = PacketDirection.FORWARD

        if self.output_mode != "csv":
            if "TCP" not in packet:
                return
            elif "UDP" not in packet:
                return

        try:
            # Creates a key variable to check
            packet_flow_key = get_packet_flow_key(packet, direction)
            flow = self.flows.get((packet_flow_key, count))
        except Exception:
            return

        self.packets_count += 1

        # If there is no forward flow with a count of 0
        if flow is None:
            # There might be one of it in reverse
            direction = PacketDirection.REVERSE
            packet_flow_key = get_packet_flow_key(packet, direction)
            flow = self.flows.get((packet_flow_key, count))

        if flow is None:
            # If no flow exists create a new flow
            direction = PacketDirection.FORWARD
            flow = Flow(packet, direction)
            packet_flow_key = get_packet_flow_key(packet, direction)
            self.flows[(packet_flow_key, count)] = flow

        elif (packet.time - flow.latest_timestamp) > EXPIRED_UPDATE:
            # If the packet exists in the flow but the packet is sent
            # after too much of a delay than it is a part of a new flow.
            expired = EXPIRED_UPDATE
            while (packet.time - flow.latest_timestamp) > expired:
                count += 1
                expired += EXPIRED_UPDATE
                flow = self.flows.get((packet_flow_key, count))

                if flow is None:
                    flow = Flow(packet, direction)
                    self.flows[(packet_flow_key, count)] = flow
                    break
        elif "F" in packet.flags:
            # If it has FIN flag then early collect flow and continue
            flow.add_packet(packet, direction)
            self.garbage_collect(packet.time)
            return

        flow.add_packet(packet, direction)

        if self.packets_count % GARBAGE_COLLECT_PACKETS == 0 or flow.duration > 120:
            self.garbage_collect(packet.time)

    def IPAddress(self, IP: str) -> str:
        return False if (ip_address(IP).is_private) else True

    def get_flows(self) -> list:
        return self.flows.values()

    def is_internal_ip(self, ip_address):
        # Xác định xem địa chỉ IP có thuộc phạm vi mạng nội bộ không
        # Ví dụ: bạn có thể kiểm tra địa chỉ IP có nằm trong dải địa chỉ mạng nội bộ của bạn hay không
        # Nếu có, trả về True; ngược lại, trả về False
        # Đây là một ví dụ giả định, bạn cần thay đổi nó phù hợp với cấu hình mạng của bạn
        internal_ip_ranges = ["192.168.0.0/16", "10.0.0.0/8", "172.16.0.0/12", "127.0.0.1", "192.168.1.10",
                              "172.19.0.1",
                              "172.26.0.1",
                              "172.17.0.1",
                              "10.2.0.1",
                              "172.18.0.1",
                              "172.24.0.1",
                              "172.25.0.1"

                              ]
        for internal_range in internal_ip_ranges:
            if IPAddress(ip_address) in IPNetwork(internal_range):
                return True
        return False

    def garbage_collect(self, latest_time) -> None:
        # TODO: Garbage Collection / Feature Extraction should have a separate thread
        self.logger.debug(f"Garbage Collection Began. Flows = {len(self.flows)}")
        keys = list(self.flows.keys())
        for k in keys:
            flow = self.flows.get(k)

            if (
                latest_time is not None
                and latest_time - flow.latest_timestamp < EXPIRED_UPDATE
                and flow.duration < 90
            ):
                continue
        # Kiểm tra xem địa chỉ nguồn có thuộc mạng nội bộ không
        # and not  self.IPAddress(flow.src_ip)
            if not self.is_internal_ip(flow.src_ip):
                self.logger.debug(f"Prepared Garbage Collection . Flows = ")
                packet = flow.get_data()
                jsonPush = json.dumps(packet)
                push_data_rabbitmq(jsonPush)
                # self.push_data_rabbitmq(jsonPush)
                # self.output_writer.write(packet)
                del self.flows[k]
            self.logger.debug(f"Garbage Collection Finished. Flows = {len(self.flows)}")


def generate_session_class(output_mode, output_file, verbose):
    return type(
        "NewFlowSession",
        (FlowSession,),
        {
            "output_mode": output_mode,
            "output_file": output_file,
            "verbose": verbose,
        },
    )
