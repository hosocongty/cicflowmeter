from scapy.packet import Packet
from scapy.layers.l2 import Ether

# from . import constants
from features.context import PacketDirection, get_packet_flow_key
from features.flag_count import FlagCount
from features.flow_bytes import FlowBytes
from features.packet_count import PacketCount
from features.packet_length import PacketLength
from features.packet_time import PacketTime
from constants.utils import get_statistics

EXPIRED_UPDATE = 240
CLUMP_TIMEOUT = 0.001
ACTIVE_TIMEOUT = 0.005
BULK_BOUND = 4
GARBAGE_COLLECT_PACKETS = 1000
class Flow:
    """This class summarizes the values of the features of the network flows"""

    def __init__(self, packet: Packet, direction: PacketDirection):
        """This method initializes an object from the Flow class.

        Args:
            packet (Any): A packet from the network.
            direction (Enum): The direction the packet is going ove the wire.
        """

        (
            self.dest_ip,
            self.src_ip,
            self.src_port,
            self.dest_port,
        ) = get_packet_flow_key(packet, direction)

        self.packets = []
        self.flow_interarrival_time = []
        self.latest_timestamp = 0
        self.start_timestamp = 0
        self.init_window_size = {
            PacketDirection.FORWARD: 0,
            PacketDirection.REVERSE: 0,
        }

        self.start_active = 0
        self.last_active = 0
        self.active = []
        self.idle = []

        self.forward_bulk_last_timestamp = 0
        self.forward_bulk_start_tmp = 0
        self.forward_bulk_count = 0
        self.forward_bulk_count_tmp = 0
        self.forward_bulk_duration = 0
        self.forward_bulk_packet_count = 0
        self.forward_bulk_size = 0
        self.forward_bulk_size_tmp = 0
        self.backward_bulk_last_timestamp = 0
        self.backward_bulk_start_tmp = 0
        self.backward_bulk_count = 0
        self.backward_bulk_count_tmp = 0
        self.backward_bulk_duration = 0
        self.backward_bulk_packet_count = 0
        self.backward_bulk_size = 0
        self.backward_bulk_size_tmp = 0

    def get_data(self) -> dict:
        """This method obtains the values of the features extracted from each flow.

        Note:
            Only some of the network data plays well together in this list.
            Time-to-live values, window values, and flags cause the data to
            separate out too much.

        Returns:
           list: returns a List of values to be outputted into a csv file.

        """

        flow_bytes = FlowBytes(self)
        flag_count = FlagCount(self)
        packet_count = PacketCount(self)
        packet_length = PacketLength(self)
        packet_time = PacketTime(self)
        flow_iat = get_statistics(self.flow_interarrival_time)
        forward_iat = get_statistics(
            packet_time.get_packet_iat(PacketDirection.FORWARD)
        )
        backward_iat = get_statistics(
            packet_time.get_packet_iat(PacketDirection.REVERSE)
        )
        active_stat = get_statistics(self.active)
        idle_stat = get_statistics(self.idle)

            # Obtain MAC addresses
        src_mac = self.get_src_mac()
        dest_mac = self.get_dest_mac()
        data = {
               # Basic IP information
            "Source IP": self.src_ip,
            "Destination IP": self.dest_ip,
            "Source Port": self.src_port,
            "Destination Port": self.dest_port,
            "Protocol": self.protocol,
            "MAC Source": src_mac,
            "MAC Destination":dest_mac,
            # Basic information from packet times
            "TimeSpan": packet_time.get_timestamp(),
            "Destination Port": self.dest_port,
            "Flow Duration": 1e6 * packet_time.get_duration(),
            "Total Fwd Packets": packet_count.get_total(PacketDirection.FORWARD),
            "Total Backward Packets": packet_count.get_total(PacketDirection.REVERSE),
            "Total Length of Fwd Packets": packet_length.get_total(PacketDirection.FORWARD),
            "Total Length of Bwd Packets": packet_length.get_total(PacketDirection.REVERSE),
            "Fwd Packet Length Max": packet_length.get_max(PacketDirection.FORWARD),
            "Fwd Packet Length Min": packet_length.get_min(PacketDirection.FORWARD),
            "Fwd Packet Length Mean":  packet_length.get_mean(PacketDirection.FORWARD),
            "Fwd Packet Length Std": packet_length.get_std(PacketDirection.FORWARD),
            "Bwd Packet Length Max": packet_length.get_max(PacketDirection.REVERSE),
            "Bwd Packet Length Min": packet_length.get_min(PacketDirection.REVERSE),
            "Bwd Packet Length Mean": packet_length.get_mean(PacketDirection.REVERSE),
            "Bwd Packet Length Std": packet_length.get_std(PacketDirection.REVERSE),
            "Flow Bytes/s": flow_bytes.get_rate(),
            "Flow Packets/s": packet_count.get_rate(),
            "Flow IAT Mean": flow_iat["mean"],
            "Flow IAT Std": flow_iat["std"],
            "Flow IAT Max": flow_iat["max"],
            "Flow IAT Min": flow_iat["min"],
            "Fwd IAT Total": forward_iat["total"],
            "Fwd IAT Mean": forward_iat["mean"],
            "Fwd IAT Std": forward_iat["std"],
            "Fwd IAT Max": forward_iat["max"],
            "Fwd IAT Min": forward_iat["min"],
            "Bwd IAT Total": backward_iat["total"],
            "Bwd IAT Mean": backward_iat["mean"],
            "Bwd IAT Std": backward_iat["std"],
            "Bwd IAT Max": backward_iat["max"],
            "Bwd IAT Min": backward_iat["min"],
            "Fwd PSH Flags": flag_count.count("PSH", PacketDirection.FORWARD),
            "Bwd PSH Flags": flag_count.count("PSH", PacketDirection.REVERSE),
            "Fwd URG Flags": flag_count.count("URG", PacketDirection.FORWARD),
            "Bwd URG Flags": flag_count.count("URG", PacketDirection.REVERSE),
            "Fwd Header Length": flow_bytes.get_forward_header_bytes(),
            "Bwd Header Length": flow_bytes.get_reverse_header_bytes(),
            "Fwd Packets/s": packet_count.get_rate(PacketDirection.FORWARD),
            "Bwd Packets/s": packet_count.get_rate(PacketDirection.REVERSE),
            "Min Packet Length": packet_length.get_min(),
            "Max Packet Length": packet_length.get_max(),
            "Packet Length Mean": packet_length.get_mean(),
            "Packet Length Std": packet_length.get_std(),
            "Packet Length Variance": packet_length.get_var(),
            "FIN Flag Count": flag_count.count("FIN"),
            "SYN Flag Count": flag_count.count("SYN"),
            "RST Flag Count": flag_count.count("RST"),
            "PSH Flag Count": flag_count.count("PSH"),
            "ACK Flag Count": flag_count.count("ACK"),
            "URG Flag Count": flag_count.count("URG"),
            "CWE Flag Count": flag_count.count("CWE"),
            "ECE Flag Count": flag_count.count("ECE"),
            "Down/Up Ratio": packet_count.get_down_up_ratio(),
            "Average Packet Size":packet_length.get_avg(),
            "Avg Fwd Segment Size": packet_length.get_mean(PacketDirection.FORWARD),
            "Avg Bwd Segment Size": packet_length.get_mean(PacketDirection.REVERSE),
            "Fwd Header Length.1": flow_bytes.get_forward_header_bytes(),
            "Fwd Avg Bytes/Bulk": flow_bytes.get_bytes_per_bulk(PacketDirection.FORWARD),
            "Fwd Avg Packets/Bulk": flow_bytes.get_packets_per_bulk(PacketDirection.FORWARD),
            "Fwd Avg Bulk Rate": flow_bytes.get_bulk_rate(PacketDirection.FORWARD),
            "Bwd Avg Bytes/Bulk": flow_bytes.get_bytes_per_bulk(PacketDirection.REVERSE),
            "Bwd Avg Packets/Bulk": flow_bytes.get_packets_per_bulk(PacketDirection.REVERSE),
            "Bwd Avg Bulk Rate": flow_bytes.get_bulk_rate(PacketDirection.REVERSE),
            "Subflow Fwd Packets": packet_count.get_total(PacketDirection.FORWARD),
            "Subflow Fwd Bytes": packet_length.get_total(PacketDirection.FORWARD),
            "Subflow Bwd Packets": packet_count.get_total(PacketDirection.REVERSE),
            "Subflow Bwd Bytes": packet_length.get_total(PacketDirection.REVERSE),
            "Init_Win_bytes_forward": self.init_window_size[PacketDirection.FORWARD],
            "Init_Win_bytes_backward": self.init_window_size[PacketDirection.REVERSE],
            "act_data_pkt_fwd": packet_count.has_payload(PacketDirection.FORWARD),
            "min_seg_size_forward": flow_bytes.get_min_forward_header_bytes(),
            "Active Mean": active_stat["mean"],
            "Active Std": active_stat["std"],
            "Active Max": active_stat["max"],
            "Active Min": active_stat["min"],
            "Idle Mean": idle_stat["mean"],
            "Idle Std": idle_stat["std"],
            "Idle Max": idle_stat["max"],
            "Idle Min": idle_stat["min"]
        }
        return data

    def add_packet(self, packet: Packet, direction: PacketDirection) -> None:
        """Adds a packet to the current list of packets.

        Args:
            packet: Packet to be added to a flow
            direction: The direction the packet is going in that flow

        """
        self.packets.append((packet, direction))

        self.update_flow_bulk(packet, direction)
        self.update_subflow(packet)

        if self.start_timestamp != 0:
            self.flow_interarrival_time.append(
                1e6 * (packet.time - self.latest_timestamp)
            )

        self.latest_timestamp = max(packet.time, self.latest_timestamp)

        if "TCP" in packet:
            if (
                direction == PacketDirection.FORWARD
                and self.init_window_size[direction] == 0
            ):
                self.init_window_size[direction] = packet["TCP"].window
            elif direction == PacketDirection.REVERSE:
                self.init_window_size[direction] = packet["TCP"].window

        # First packet of the flow
        if self.start_timestamp == 0:
            self.start_timestamp = packet.time
            self.protocol = packet.proto

    def update_subflow(self, packet: Packet):
        """Update subflow

        Args:
            packet: Packet to be parse as subflow

        """
        last_timestamp = (
            self.latest_timestamp if self.latest_timestamp != 0 else packet.time
        )
        if (packet.time - last_timestamp) > CLUMP_TIMEOUT:
            self.update_active_idle(packet.time - last_timestamp)

    def update_active_idle(self, current_time):
        """Adds a packet to the current list of packets.

        Args:
            packet: Packet to be update active time

        """
        if (current_time - self.last_active) > ACTIVE_TIMEOUT:
            duration = abs(float(self.last_active - self.start_active))
            if duration > 0:
                self.active.append(1e6 * duration)
            self.idle.append(1e6 * (current_time - self.last_active))
            self.start_active = current_time
            self.last_active = current_time
        else:
            self.last_active = current_time

    def update_flow_bulk(self, packet: Packet, direction: PacketDirection):
        """Update bulk flow

        Args:
            packet: Packet to be parse as bulk

        """
        payload_size = len(PacketCount.get_payload(packet))
        if payload_size == 0:
            return
        if direction == PacketDirection.FORWARD:
            if self.backward_bulk_last_timestamp > self.forward_bulk_start_tmp:
                self.forward_bulk_start_tmp = 0
            if self.forward_bulk_start_tmp == 0:
                self.forward_bulk_start_tmp = packet.time
                self.forward_bulk_last_timestamp = packet.time
                self.forward_bulk_count_tmp = 1
                self.forward_bulk_size_tmp = payload_size
            else:
                if (
                    packet.time - self.forward_bulk_last_timestamp
                ) > CLUMP_TIMEOUT:
                    self.forward_bulk_start_tmp = packet.time
                    self.forward_bulk_last_timestamp = packet.time
                    self.forward_bulk_count_tmp = 1
                    self.forward_bulk_size_tmp = payload_size
                else:  # Add to bulk
                    self.forward_bulk_count_tmp += 1
                    self.forward_bulk_size_tmp += payload_size
                    if self.forward_bulk_count_tmp == BULK_BOUND:
                        self.forward_bulk_count += 1
                        self.forward_bulk_packet_count += self.forward_bulk_count_tmp
                        self.forward_bulk_size += self.forward_bulk_size_tmp
                        self.forward_bulk_duration += (
                            packet.time - self.forward_bulk_start_tmp
                        )
                    elif self.forward_bulk_count_tmp > BULK_BOUND:
                        self.forward_bulk_packet_count += 1
                        self.forward_bulk_size += payload_size
                        self.forward_bulk_duration += (
                            packet.time - self.forward_bulk_last_timestamp
                        )
                    self.forward_bulk_last_timestamp = packet.time
        else:
            if self.forward_bulk_last_timestamp > self.backward_bulk_start_tmp:
                self.backward_bulk_start_tmp = 0
            if self.backward_bulk_start_tmp == 0:
                self.backward_bulk_start_tmp = packet.time
                self.backward_bulk_last_timestamp = packet.time
                self.backward_bulk_count_tmp = 1
                self.backward_bulk_size_tmp = payload_size
            else:
                if (
                    packet.time - self.backward_bulk_last_timestamp
                ) > CLUMP_TIMEOUT:
                    self.backward_bulk_start_tmp = packet.time
                    self.backward_bulk_last_timestamp = packet.time
                    self.backward_bulk_count_tmp = 1
                    self.backward_bulk_size_tmp = payload_size
                else:  # Add to bulk
                    self.backward_bulk_count_tmp += 1
                    self.backward_bulk_size_tmp += payload_size
                    if self.backward_bulk_count_tmp == BULK_BOUND:
                        self.backward_bulk_count += 1
                        self.backward_bulk_packet_count += self.backward_bulk_count_tmp
                        self.backward_bulk_size += self.backward_bulk_size_tmp
                        self.backward_bulk_duration += (
                            packet.time - self.backward_bulk_start_tmp
                        )
                    elif self.backward_bulk_count_tmp > BULK_BOUND:
                        self.backward_bulk_packet_count += 1
                        self.backward_bulk_size += payload_size
                        self.backward_bulk_duration += (
                            packet.time - self.backward_bulk_last_timestamp
                        )
                    self.backward_bulk_last_timestamp = packet.time
    
    def get_mac_addresses_by_source_ip(self):
        """Get MAC addresses for each source IP in the flow."""
        mac_addresses = {}

        for packet, direction in self.packets:
            if Ether in packet:
                src_ip = packet[IP].src
                src_mac = packet[Ether].src
                if src_ip not in mac_addresses:
                    mac_addresses[src_ip] = []
                mac_addresses[src_ip].append(src_mac)

        return mac_addresses
    
    def get_src_mac(self):
        """Get the source MAC address."""
        for packet, direction in self.packets:
            if Ether in packet:
                return packet[Ether].src
        return "Unknown"

    def get_dest_mac(self):
        """Get the destination MAC address."""
        for packet, direction in self.packets:
            if Ether in packet:
                return packet[Ether].dst
        return "Unknown"
    @property
    def duration(self):
        return self.latest_timestamp - self.start_timestamp
