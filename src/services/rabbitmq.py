import pika

import os
import logging
import time

logging.basicConfig()


    
def push_data_rabbitmq(data):
        print(f"Prepare Data push RabbitMQ: {data}")
        # Parse CLOUDAMQP_URL (fallback to localhost)
        url = os.environ.get('CLOUDAMQP_URL', 'amqp://huy:NTH@13042000@localhost/')
        params = pika.URLParameters(url)
        params.socket_timeout = 5

        # Specify the virtual host explicitly
        params.virtual_host = '/'

        try:
            # Connect to RabbitMQ
            connection = pika.BlockingConnection(params)
            print("Successfully connected to RabbitMQ")

            # Create a new channel
            channel = connection.channel()

            # Main loop to publish messages every 10 seconds
            channel.queue_declare(queue='dos_queue', durable=True)
                # Ensure the queue\
                    # exists
                
                # Send a message
            channel.basic_publish(exchange='dos_exchange', routing_key='dos_process', body=data, properties=pika.BasicProperties(delivery_mode=2))  # Set message persistence
            print("[x] Message sent to consumer")

                # Sleep for 10 seconds
            time.sleep(1)

        except pika.exceptions.AMQPConnectionError:
            print("Failed to connect to RabbitMQ")

        finally:
            # Close the connection
            if connection and connection.is_open:
                connection.close()

# push_data_rabbitmq("HUYTESTT")